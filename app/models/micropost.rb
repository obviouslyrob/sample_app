class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size
  
  private
  
  # Validates the size of the picutre
  def picture_size
    if picture.size > 4.megabytes
      errors.add(:picture, "Should be less than 5 MB")
    end
  end
end
